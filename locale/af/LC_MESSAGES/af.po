# Afrikaans translations for LogOutButton package
# Afrikaans messages for LogOutButton.
# Copyright (C) 2016 Kyle Robbertze <kyle@aims.ac.za>
# This file is distributed under the same license as the LogOutButton package.
# Kyle Robbertze <kyle@aims.ac.za>, 2016
#
msgid ""
msgstr ""
"Project-Id-Version: LogOutButton 1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-12-01 11:24+0200\n"
"PO-Revision-Date: 2016-12-01 11:15+0200\n"
"Last-Translator: Kyle Robbertze <kyle@aims.ac.za>\n"
"Language-Team: Afrikaans\n"
"Language: af\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 1.8.9\n"

#: ../src/extension.js:16
msgid "Log Out"
msgstr "Teken Uit"
